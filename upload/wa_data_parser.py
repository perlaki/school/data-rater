#!/usr/bin/env python3
import sys
import re
from pathlib import Path
from datetime import datetime
import json

wa_patter = re.compile(
    r'^\[(?P<day>0[1-9]|[12][0-9]|3[01])\.(?P<month>0[1-9]|1[012])\.(?P<year>\d{2}), (?P<hour>[01][0-9]|2[0-3]):(?P<minute>[0-5][0-9]):(?P<second>[0-5][0-9])\] (?P<sender>[^:]*): (?P<message>.*)$', re.MULTILINE)


def form(match):
    dat = match.groupdict()
    return {
        'sender': dat['sender'].translate({8234: None, 160: None, 8236: None}),
        'message': dat['message'],
        'datetime': str(datetime(2000+int(dat['year']), int(dat['month']), int(dat['day']), int(dat['hour']), int(dat['minute']), int(dat['second'])))
    }


path = Path(sys.argv[1] if len(sys.argv) >=
            2 else input('Whatsapp exportfile: '))

if path.is_file():
    with open(path) as file:
        json.dump(list(map(form, wa_patter.finditer(file.read()))),
                  path.with_suffix('.json').open('w'), indent=4)
