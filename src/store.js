import Vue from 'vue'
import Vuex from 'vuex'

import {
  parse,
  Data,
  User,
} from './parse'
import {
  ulid
} from 'ulid';


Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    state: '',
    obj: new Data({
      message: 'Welcome to Amy',
      init: true
    }),
  },
  getters: {
    user: () => User.current(),
    isSplash: (state) => state.obj.has('init'),
    message: (state) => state.obj.get('message'),
    obj: (state) => state.obj,
    query: (state, getters) => new parse.Query(Data).limit(1).notEqualTo('readBy', getters.user).ascending('objectId'),
    done: (state) => state.state === 'done',
    loading: (state) => state.state === 'loading',
  },
  mutations: {
    load(state) {
      state.state = 'loading';
    },
    done(state, [obj]) {
      state.obj = obj;
      state.state = 'done';
    },
    setUser(state, user) {
      state.user = user;
      state.state = 'logedin';
    },
    rate(state, rating) {
      if (!state.obj.has('rating')) state.obj.set('rating', [])
      state.obj.set('rating', [rating, ...state.obj.get('rating')])
      state.state = 'rated'
    },
    addUser(state, user) {
      if (!state.obj.has('readBy')) state.obj.set('readBy', [])
      state.obj.set('readBy', [user, ...state.obj.get('readBy')])
    },
    save(state) {
      return state.obj.save()
    }
  },
  actions: {
    async login({
      commit,
      getters
    }) {
      commit('load');
      if (!getters.user) commit('setUser', await (new User)._linkWith('anonymous', {
        authData: {
          id: ulid()
        }
      }))
    },
    async load({
      commit,
      getters,
      dispatch
    }) {
      commit('load')
      commit('done', await getters.query.find())
      dispatch('addCurrentUser')
    },
    async accept({
      commit,
      dispatch,
      getters
    }) {
      commit('rate', 'accept')
      if (!getters.isSplash) await commit('save')
      dispatch('load')
    },
    async reject({
      commit,
      dispatch,
      getters
    }) {
      commit('rate', 'reject')
      if (!getters.isSplash) await commit('save')
      dispatch('load')
    },
    addCurrentUser({
      commit,
      getters
    }) {
      commit('addUser', getters.user)
    }
  }
})