import parse from 'parse';

parse.serverURL = 'https://data-rater.back4app.io'; // This is your Server URL
parse.initialize(
    '6Bd4sM0Ygu682Bbhk0ukKE72sGwTGDYHYxn89Nhe', // This is your Application ID
    '4DV7AHY0nPQmmMsWbaUclFK15eVp126F0NIj88py' // This is your Javascript key
);

export default parse;


const Data = parse.Object.extend('Data');
const User = parse.User


export {
    parse,
    Data,
    User,
};